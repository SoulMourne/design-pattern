/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import tp2.BoutDehors;
import tp2.Velo;
import tp1.Voilier;
import tp1.Alizes;
import tp1.Multicoque;
import tp1.Orthodromie;
import tp1.Monocoque;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jgoodwin
 */
public class VoilierTest {
    
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    Voilier monocoque1 = new Monocoque("Mono-bateau", new Orthodromie());
    Voilier multicoque1 = new Multicoque("Multi-bateau", new Alizes());
    
    @Before
    public void setUpStreams() 
    {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() 
    {
    System.setOut(null);
    System.setErr(null);
    }
    
    @Test
    public void monocoqueToStringTest() 
    {
        String monoString = "Monocoque - Mono-bateau\n";
        assertTrue(monoString+" = "+monocoque1.toString(), monoString.equals(monocoque1.toString()));
    }
    
    @Test
    public void multicoqueToStringTest()
    {
        String multiString = "Multicoque - Multi-bateau\n";
        assertTrue(multiString+" = "+multicoque1.toString(),multiString.equals(multicoque1.toString()));
    }
    
    @Test
    public void monocoqueAppliqueRouteTest()
    {
        monocoque1.appliqueSuivreRoute();
        assertEquals("Je suis l'orthodromie.\n", outContent.toString());
    }
    
    @Test
    public void multicoqueAppliqueRouteTest()
    {
        multicoque1.appliqueSuivreRoute();
        assertEquals("Je suis la route des Alizés.\n", outContent.toString());
    }
    
    @Test
    public void multicoqueChangeRoute()
    {
        multicoque1.setSuivreRoute(new Orthodromie());
        multicoque1.appliqueSuivreRoute();
        assertEquals("Je suis l'orthodromie.\n", outContent.toString());
    }
    
    @Test
    public void monocoqueChangeRoute()
    {
        monocoque1.setSuivreRoute(new Alizes());
        monocoque1.appliqueSuivreRoute();
        assertEquals("Je suis la route des Alizés.\n", outContent.toString());
    }
    
    @Test
    public void monocoqueAjoutVelo()
    {
        monocoque1 = new Velo(monocoque1);
        String expectedString = "Monocoque - Mono-bateau\nPossède un vélo\n";
        assertTrue(expectedString+" = "+monocoque1.toString(),expectedString.equals(monocoque1.toString()));
    }
    
    @Test
    public void multicoqueAjoutVelo()
    {
        multicoque1 = new Velo(multicoque1);
        String expectedString = "Multicoque - Multi-bateau\nPossède un vélo\n";
        assertTrue(expectedString+" = "+multicoque1.toString(),expectedString.equals(multicoque1.toString()));
    }
    
    @Test
    public void monocoqueAjoutDeuxDecorateur()
    {
        monocoque1 = new Velo(monocoque1);
        monocoque1 = new BoutDehors(monocoque1);
        String expectedString = "Monocoque - Mono-bateau\nPossède un vélo\nPossède un bout dehors\n";
        assertTrue(expectedString+" = "+monocoque1.toString(), expectedString.equals(monocoque1.toString()));
    }
    
    @Test
    public void multicoqueAjoutDeuxDecorateur()
    {
        multicoque1 = new Velo(multicoque1);
        multicoque1 = new BoutDehors(multicoque1);
        String expectedString = "Multicoque - Multi-bateau\nPossède un vélo\nPossède un bout dehors\n";
        assertTrue(expectedString+" = "+multicoque1.toString(), expectedString.equals(multicoque1.toString()));
    }
    
    @Test
    public void monocoqueAjoutVeloEtAppliqueRoute()
    {
        monocoqueAppliqueRouteTest();
        monocoqueAjoutDeuxDecorateur();
        monocoque1.setSuivreRoute(new Alizes());
        System.out.print("Changement de route.\n");
        monocoque1.appliqueSuivreRoute();
        assertEquals("Je suis l'orthodromie.\nChangement de route.\nJe suis la route des Alizés.\n", outContent.toString());
    }
    
    @Test
    public void multicoqueAjoutVeloEtAppliqueRoute()
    {
        multicoqueAppliqueRouteTest();
        multicoqueAjoutDeuxDecorateur();
        multicoque1.setSuivreRoute(new Orthodromie());
        System.out.print("Changement de route.\n");
        multicoque1.appliqueSuivreRoute();
        assertEquals("Je suis la route des Alizés.\nChangement de route.\nJe suis l'orthodromie.\n", outContent.toString());
    }
}