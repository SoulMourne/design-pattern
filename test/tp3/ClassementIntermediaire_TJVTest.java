package tp3;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tp3.ClassementIntermediaire_TJV;

public class ClassementIntermediaire_TJVTest 
{
    @Test
    public void lireFichier1() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_10_25.xml");
        assertTrue(classement!=null);
    }
    
    @Test
    public void lireFichier2() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_10_29.xml");
        assertTrue(classement!=null);
    }
    
    @Test
    public void lireFichier3() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_11_03.xml");
        assertTrue(classement!=null);
    }
    
    @Test
    public void lireFichier4() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_11_08.xml");
        assertTrue(classement!=null);
    }
    
    @Test
    public void lireFichier5() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_11_11.xml");
        assertTrue(classement!=null);
    }
    
    
    @Test
    public void lireFichier6() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_11_15.xml");
        assertTrue(classement!=null);
    }
    
    @Test
    public void lireFichier7() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_11_19.xml");
        assertTrue(classement!=null);
    }
    
    
    @Test
    public void lireFichier8() 
    {
        ClassementIntermediaire_TJV classement = 
                new ClassementIntermediaire_TJV("src/tp3/xml_files/classement_2015_11_27.xml");
        assertTrue(classement!=null);
    }
    
    @Test
    public void lireTousLesFichiers()
    {
        TousLesClassements tousLesClassements = new TousLesClassements();
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_25.xml");
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_29.xml");
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_03.xml");
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_08.xml");
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_11.xml");
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_15.xml");
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_19.xml");
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_27.xml");
        System.out.println(tousLesClassements.toString());
    }
    
//    @Test
//    public void lireAbandons()
//    {
//        TousLesClassements tousLesClassements = new TousLesClassements();
//        tousLesClassements.addObservateur(new Abandons());
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_25.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_29.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_03.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_08.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_11.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_15.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_19.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_27.xml");
//        Abandons abandons = (Abandons)tousLesClassements.getObservateurs().get(0);
//        System.out.println(abandons.toString());
//    }
    
//    @Test
//    public void lireArrivee()
//    {
//        TousLesClassements tousLesClassements = new TousLesClassements();
//        tousLesClassements.addObservateur(new Arrivees());
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_25.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_29.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_03.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_08.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_11.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_15.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_19.xml");
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_27.xml");
//        Arrivees arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println(arrivees.toString());
//    }
//    
//    @Test
//    public void lireAbandonsEtArrivees()
//    {
//        TousLesClassements tousLesClassements = new TousLesClassements();
//        tousLesClassements.addObservateur(new Arrivees());
//        tousLesClassements.addObservateur(new Abandons());
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_25.xml");
//        Abandons abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        Arrivees arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println("25/10/2015\n");
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_29.xml");
//        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println("29/10/2015\n");
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_03.xml");
//        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        System.out.println("03/11/2015\n");
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_08.xml");
//        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        System.out.println("08/11/2015\n");
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_11.xml");
//        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        System.out.println("11/11/2015\n");
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_15.xml");
//        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        System.out.println("11/15/2015\n");
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_19.xml");
//        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        System.out.println("11/19/2015\n");
//        
//        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_27.xml");
//        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
//        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
//        System.out.println(arrivees.toString());
//        System.out.println(abandons.toString());
//        System.out.println("27/11/2015\n");
//    }
    
    @Test
    public void lireNumAbandonsEtArrivees()
    {
        TousLesClassements tousLesClassements = new TousLesClassements();
        tousLesClassements.addObservateur(new Arrivees());
        tousLesClassements.addObservateur(new Abandons());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_25.xml");
        Abandons abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        Arrivees arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("25/10/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_10_29.xml");
        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("29/10/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_03.xml");
        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("03/11/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_08.xml");
        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("08/11/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_11.xml");
        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("11/11/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_15.xml");
        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("11/15/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_19.xml");
        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("11/19/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
        
        tousLesClassements.ajoutClassement("src/tp3/xml_files/classement_2015_11_27.xml");
        abandons = (Abandons)tousLesClassements.getObservateurs().get(1);
        arrivees = (Arrivees)tousLesClassements.getObservateurs().get(0);
        System.out.println("27/11/2015\n");
        System.out.println("Arrivées : "+arrivees.getArrivees().size() +"\nAbandons : "+abandons.getAbandons().size());
    }
}
