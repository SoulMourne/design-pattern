package tp4;

import tp1.Monocoque;

public class IMOCA extends Monocoque
{
    public IMOCA(String parNom) {
        super(parNom);
    }
    
    @Override
    public String toString()
    {
        String retour = super.toString();
        retour += "Je suis un Imoca";
        return retour;
    }
}
