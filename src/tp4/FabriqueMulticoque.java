package tp4;

import tp1.Multicoque;
import tp1.Voilier;
import tp4.Participants_TJV.ClassesTJV2015;

public class FabriqueMulticoque implements FabriqueVoilier
{
    public FabriqueMulticoque()
    {
    }
    
    @Override
    public Multicoque factoryMethod(ClassesTJV2015 classe,String parNom) 
    {
        switch(classe)
        {
            case ULTIME:
                return new ULTIME(parNom);
            case MULTI50:
                return new MULTI50(parNom);
        }
        return null;
    }
}
