package tp4;

import tp1.Voilier;
import tp4.Participants_TJV.ClassesTJV2015;

public interface FabriqueVoilier 
{
    public Voilier factoryMethod(ClassesTJV2015 classe,String parNom);
}
