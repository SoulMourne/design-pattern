package tp4;

import tp1.Multicoque;

public class MULTI50 extends Multicoque
{
    public MULTI50(String parNom) 
    {
        super(parNom);
    }
    
    @Override
    public String toString()
    {
        String retour = super.toString();
        retour += "Je suis un Multi_50";
        return retour;
    }
}
