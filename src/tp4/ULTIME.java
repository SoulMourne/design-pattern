package tp4;

import tp1.Multicoque;
import tp1.SuivreRoute;

public class ULTIME extends Multicoque
{
    public ULTIME(String parNom) 
    {
        super(parNom);
    }
    
    @Override
    public String toString()
    {
        String retour = super.toString();
        retour += "Je suis un Ultime";
        return retour;
    }
}
