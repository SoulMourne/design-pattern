package tp4;

import tp1.Monocoque;

public class CLASS40 extends Monocoque
{
    public CLASS40(String parNom) {
        super(parNom);
    }
    
    @Override
    public String toString()
    {
        String retour = super.toString();
        retour += "Je suis un Class40";
        return retour;
    }
}
