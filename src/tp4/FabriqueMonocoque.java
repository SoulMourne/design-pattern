package tp4;

import tp1.Monocoque;
import tp1.Voilier;
import tp4.Participants_TJV.ClassesTJV2015;

public class FabriqueMonocoque implements FabriqueVoilier
{
    public FabriqueMonocoque()
    {
    }
    
    @Override
    public Monocoque factoryMethod(ClassesTJV2015 classe, String parNom) 
    {
        switch(classe)
        {
            case CLASS40:
                return new CLASS40(parNom);
            case IMOCA:
                return new IMOCA(parNom);
        }
        return null;
    }
}
