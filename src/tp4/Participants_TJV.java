package tp4;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tp1.Voilier;

public class Participants_TJV 
{
    public enum ClassesTJV2015
    {
        CLASS40,
        MULTI50,
        IMOCA,
        ULTIME
    }
    
    HashMap <String, Voilier> inscrits = new HashMap<>();
    TreeSet <String> [] inscritsParClasse = new TreeSet [ClassesTJV2015.values().length];
    
    public Participants_TJV(String adresseFichier ) 
    {
        lectureFichierXML(adresseFichier);
        for(int i = 0;i<this.inscritsParClasse.length;i++)
            inscritsParClasse[i] = new TreeSet<>();
        inscritsParClasse ();
    }
    
    public void lectureFichierXML (String adresse)
    {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = builderFactory.newDocumentBuilder();
            Document document = builder.parse(new FileInputStream(adresse));
            NodeList listeClasses = document.getElementsByTagName("classe");
            for (int i=0 ; i < listeClasses.getLength() ; i++) 
            {
                Element classeCourante = (Element) listeClasses.item(i);
                String classe = classeCourante.getAttribute("nom");
                ClassesTJV2015 classeEnum = ClassesTJV2015.valueOf (classe);
                NodeList listeVoiliers = classeCourante.getElementsByTagName("releve");
                for (int j= 0 ; j< listeVoiliers.getLength() ; j++) 
                {
                    Element releveCourant = (Element) listeVoiliers.item(j);
                    String nom = releveCourant.getAttribute("nom");
                    if (!nom.equals(""))
                    {
                        String skippers = releveCourant.getAttribute("skippers");
                        Voilier voilier = createVoilier (nom, classeEnum);
                        inscrits.put (skippers,voilier);
                    }
                }
            }// for
        } //try
        catch (SAXException e){
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
    
    private Voilier createVoilier(String nom, ClassesTJV2015 classe) 
    {
        FabriqueVoilier fabrique =null;
        //System.out.println(classe);
        switch(classe)
        {
            case IMOCA: case CLASS40:
                fabrique = new FabriqueMonocoque();
                break;
            case MULTI50: case ULTIME:
                fabrique = new FabriqueMulticoque();
                break;
        }
        return fabrique.factoryMethod(classe, nom);
    }
    
    private void inscritsParClasse() 
    {
        Set <String> keySet = this.inscrits.keySet();
//        System.out.println(inscrits);
        for (String cle : keySet)
        {
            Voilier courant = this.inscrits.get(cle);
            String name = courant.getClass().toString();
            //System.out.println(name);
            switch(name)
            {
                case "class tp4.CLASS40":
                    this.inscritsParClasse[0].add(courant.getNom());
                    break;
                case "class tp4.MULTI50":
                    this.inscritsParClasse[1].add(courant.getNom());
                    break;
                case "class tp4.IMOCA":
                    this.inscritsParClasse[2].add(courant.getNom());
                    break;
                case "class tp4.ULTIME":
                    this.inscritsParClasse[3].add(courant.getNom());
                    break;
            }
        }
        for(int i = 0; i<inscritsParClasse.length;i++)
        {
            switch(i)
            {
                case 0:
                    System.out.println("CLASS40");
                    break;
                case 1:
                    System.out.println("MULTI50");
                    break;
                case 2:
                    System.out.println("IMOCA");
                    break;
                case 3:
                    System.out.println("ULTIME");
                    break;
            }
                System.out.println(inscritsParClasse[i]);
        }
    }
}
