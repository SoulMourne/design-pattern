/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tp3;

import java.util.ArrayList;
import java.util.Iterator;

public class Abandons implements Observateur
{
    private ArrayList <String> voiliersAbandons;
    
    public Abandons()
    {
        this.voiliersAbandons = new ArrayList<>();
    }
    
    public void setAbandons(ArrayList<String> parAbandons)
    {
        voiliersAbandons = parAbandons;
    }
    
    public ArrayList<String> getAbandons()
    {
        return voiliersAbandons;
    }
    
    public void addAbandon(String parVoilier)
    {
        this.voiliersAbandons.add(parVoilier);
    }

    @Override
    public void actualise(Observable o) 
    {
        TousLesClassements AllClassement = (TousLesClassements)o;
        ClassementIntermediaire_TJV classement = AllClassement.getClassementIntermediaire_TJVs().get(AllClassement.getClassementIntermediaire_TJVs().size()-1);
        Iterator iterator = classement.getClassementIntermédiaire().iterator();
        while (iterator.hasNext()) 
        {
            Position courant = (Position)iterator.next();
            if (courant.getAbandon() && !this.voiliersAbandons.contains(courant.getNom()))
            {
                this.addAbandon(courant.getNom());
            }
        }
    }
    
    public ArrayList <String> getArrivees()
    {
        return this.voiliersAbandons;
    }
    
    @Override
    public String toString()
    {
        String retour = "----------Liste abandons-----------\n";
        Iterator iterator = this.voiliersAbandons.iterator();
        while (iterator.hasNext()) 
        {
            retour += iterator.next()+"\n";
        }
        return retour;
    }
}
