package tp3;

public interface Observateur 
{
    public void actualise(Observable o);
}
