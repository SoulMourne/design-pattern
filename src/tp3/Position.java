package tp3;

import java.util.GregorianCalendar;

/**
 *
 * @author jgoodwin
 */
public class Position implements Comparable<Position>
{
    private String nomVoilier;
    private double distanceArrivee;
    private GregorianCalendar dateEtHeureArrivee;
    private boolean abandon;
    
    public Position (String parNomVoilier, double parDistanceArrivee, GregorianCalendar parDateEtHeureArrivee, boolean parAbandon)
    {
        this.nomVoilier = parNomVoilier;
        this.distanceArrivee = parDistanceArrivee;
        this.dateEtHeureArrivee = parDateEtHeureArrivee;
        this.abandon = parAbandon;
    }
    
    @Override
    public int compareTo(Position o) 
    {
        return this.nomVoilier.compareTo(o.toString());
//        if (this.getDistanceArrivee()<o.getDistanceArrivee())
//            return -1;
//        else if (this.getDistanceArrivee()>o.getDistanceArrivee())
//            return 1;
//        else
//            return 0;
    }
    
    public double getDistanceArrivee()
    {
        return this.distanceArrivee;
    }
    
    public GregorianCalendar getDateEtHeureArrivee()
    {
        return this.dateEtHeureArrivee;
    }
    
    public String getNom()
    {
        return this.nomVoilier;
    }
    
    public boolean getAbandon()
    {
        return this.abandon;
    }
    
    @Override
    public String toString()
    {
        String retour = "***Voilier :"+ this.getNom() +"***\n";
        retour += "Distance : "+this.getDistanceArrivee()+"\n";
        retour += "A abandonné : ";
        if (this.getAbandon()==true)
            retour += "Oui\n";
        else
            retour += "Non\n";
        return retour;
    }
}
