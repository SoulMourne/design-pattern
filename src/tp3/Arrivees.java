package tp3;

import java.util.ArrayList;
import java.util.Iterator;

public class Arrivees implements Observateur
{
    private ArrayList <String> voiliersArrivees;

    public Arrivees ()
    {
        this.voiliersArrivees = new ArrayList<>();
    }

    public void addArrivee(String parNom)
    {
        this.voiliersArrivees.add(parNom);
    }
    
    @Override
    public void actualise(Observable o) 
    {
        TousLesClassements AllClassement = (TousLesClassements)o;
        ClassementIntermediaire_TJV classement = AllClassement.getClassementIntermediaire_TJVs().get(AllClassement.getClassementIntermediaire_TJVs().size()-1);
        Iterator iterator = classement.getClassementIntermédiaire().iterator();
        while (iterator.hasNext()) 
        {
            Position courant = (Position)iterator.next();
            if (!courant.getAbandon() && !this.voiliersArrivees.contains(courant.getNom()) && courant.getDateEtHeureArrivee()!=null)
            {
                this.addArrivee(courant.getNom());
            }
        }
    }
    
    public ArrayList <String> getArrivees()
    {
        return this.voiliersArrivees;
    }
            
    @Override
    public String toString()
    {
        String retour = "----------Liste arrivée-----------\n";
        Iterator iterator = this.voiliersArrivees.iterator();
        while (iterator.hasNext()) 
        {
            retour += iterator.next()+"\n";
        }
        return retour;
    }
}
