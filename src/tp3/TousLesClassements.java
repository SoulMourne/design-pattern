package tp3;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author jgoodwin
 */
public class TousLesClassements implements Observable
{
    private ArrayList<ClassementIntermediaire_TJV> classements;
    private ArrayList<Observateur> observateurs;
    
    public TousLesClassements()
    {
        classements = new ArrayList<>();
        observateurs = new ArrayList<>();
    }
    
    public void ajoutClassement(String filename)
    {
        classements.add(new ClassementIntermediaire_TJV(filename));
        this.notifyObservateur();
    }
    
    @Override
    public String toString()
    {
        Iterator iterator = classements.iterator();
        String retour = "";
        while(iterator.hasNext())
        {
            retour += iterator.next().toString();
        }
        return retour;
    }

    @Override
    public void addObservateur(Observateur o) 
    {
        this.observateurs.add(o);
    }

    @Override
    public void removeObservateur(Observateur o) 
    {
        this.observateurs.remove(o);
    }

    @Override
    public void notifyObservateur() 
    {
        Iterator <Observateur> iterator = observateurs.iterator();
        while(iterator.hasNext())
        {
            iterator.next().actualise(this);
        }
    }

    public ArrayList<ClassementIntermediaire_TJV> getClassementIntermediaire_TJVs()
    {
        return this.classements;
    }
    
    public ArrayList<Observateur> getObservateurs()
    {
        return this.observateurs;
    }
}
