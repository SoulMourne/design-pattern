package tp2;


import tp1.Voilier;

/**
 *
 * @author jgoodwin
 */
public class Velo extends ArmementSupplementaire
{
    public Velo (Voilier parV)
    {
        super(parV);
    }
    
    public String toString() 
    {
        return this.getComposant().toString() + "Possède un vélo\n";
    }
}
