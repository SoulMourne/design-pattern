package tp2;


import tp1.Voilier;
import tp1.SuivreRoute;

/**
 *
 * @author jgoodwin
 */
public abstract class ArmementSupplementaire extends Voilier
{
    protected Voilier composantADecorer;
    
    public ArmementSupplementaire (Voilier parV)
    {
        super();
        this.composantADecorer = parV;
    }

    @Override
    public abstract String toString();
    
    public Voilier getComposant()
    {
        return this.composantADecorer;
    }
    
    public void setComposant(Voilier composant)
    {
        this.composantADecorer = composant;
    }
    
    @Override
    public void appliqueSuivreRoute()
    {
        this.getComposant().appliqueSuivreRoute();
    }
    
    @Override
    public void setSuivreRoute(SuivreRoute parSuivreRoute)
    {
        this.getComposant().setSuivreRoute(parSuivreRoute);
    }
    
    @Override
    public SuivreRoute getSuivreRoute()
    {
        return this.getComposant().getSuivreRoute();
    }
}
