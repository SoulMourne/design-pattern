package tp2;


import tp1.Voilier;

/**
 *
 * @author jgoodwin
 */
public class BoutDehors extends ArmementSupplementaire
{
    public BoutDehors (Voilier parV)
    {
        super(parV);
    }

    @Override
    public String toString() 
    {
        return this.getComposant().toString()+"Possède un bout dehors\n";
    }
}
