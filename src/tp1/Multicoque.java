package tp1;

/**
 *
 * @author jgoodwin
 */
public class Multicoque extends Voilier
{
    
//    public Multicoque(String parNom, SuivreRoute parSuivreRoute)
//    {
//        this.setNom(parNom);
//        this.setSuivreRoute(parSuivreRoute);
//    }
    public Multicoque(String parNom)
    {
        super(parNom);
    }
    
    public Multicoque(String parNom, SuivreRoute parSuivreRoute) {
        super(parNom, parSuivreRoute);
    }
    
    @Override
    public String toString()
    {
        String retour = super.toString();
        return "Multicoque - " + retour; 
    }
}
