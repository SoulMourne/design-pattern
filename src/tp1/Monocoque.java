package tp1;

/**
 *
 * @author jgoodwin
 */
public class Monocoque extends Voilier
{
    
//    public Monocoque(String parNom, SuivreRoute parSuivreRoute)
//    {
//        this.setNom(parNom);
//        this.setSuivreRoute(parSuivreRoute);
//    }
    
    public Monocoque(String parNom)
    {
        super(parNom);
    }
    
    public Monocoque(String parNom, SuivreRoute parSuivreRoute) {
        super(parNom, parSuivreRoute);
    }
    
    @Override
    public String toString()
    {
        String retour = super.toString();
        return "Monocoque - " + retour; 
    }
}
