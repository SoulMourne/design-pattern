package tp1;

/**
 *
 * @author jgoodwin
 */
public abstract class Voilier
{
    private String nom;
    private SuivreRoute suivreRoute;
   
    public Voilier(String parNom, SuivreRoute parSuivreRoute)
    {
        this.setNom(parNom);
        this.setSuivreRoute(parSuivreRoute);
    }
    
    public Voilier(String parNomString)
    {
        this.nom=parNomString;
    }
        
    public Voilier ()
    {
        
    }
    
    @Override
    public String toString()
    {
        return nom + "\n";
    }
    
    public SuivreRoute getSuivreRoute()
    {
        return this.suivreRoute;
    }
    
    public void setSuivreRoute(SuivreRoute parSuivreRoute)
    {
        this.suivreRoute = parSuivreRoute;
    }
    
    public String getNom()
    {
        return this.nom;
    }
    
    public void setNom(String parNom)
    {
        this.nom = parNom;
    }
    
    public void appliqueSuivreRoute()
    {
        this.getSuivreRoute().suivreRoute();
    }
}
